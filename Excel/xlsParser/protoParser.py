import os
import platform
import subprocess
import sys

import protoTmp
import utils

excel_path = os.path.join(os.getcwd(), '')

out_proto_path = os.path.join(excel_path, "output/proto")
out_py_path = os.path.join(excel_path, 'output/py')
out_binary_path = os.path.join(excel_path, 'output/datas')

meta_define = {
    'int': 'int32',
    'float': "float",
    'bool': 'bool',
    'string': 'string',
}

array_array_define = {
    'int': 'PBintArray',
    'float': "PBfloatArray",
    'bool': 'PBboolArray',
    'string': 'PBstringArray',
}


def AddMetaFieldDeclare(field_name, field_type, index, note):
    return '    %s %s = %s;  // %s\n' % (meta_define[field_type], field_name, index, note)


def AddArrayArrayDeclare(field_name, field_type, index, note):
    item_type = field_type[:-4]  # remove [][]
    return '    repeated %s %s = %s;  // %s\n' % (array_array_define[item_type], field_name, index, note)


def AddArrayFieldDeclare(field_name, field_type, index, note):
    item_type = field_type[:-2]  # remove []
    if not utils.StruckCheck(item_type):
        return '    repeated %s %s = %s;  // %s\n' % (meta_define[item_type], field_name, index, note)
    else:
        return AddStructArrayFieldDeclare(field_name, field_type, index, note)


def AddStructArrayFieldDeclare(field_name, field_type, index, note):
    names = field_name.split('|')
    types = field_type[1:-3].split('|')

    if names.__len__() != (types.__len__() + 1):
        raise Exception("struct fields and names mismatch!")

    result = '    message sub_%(name)s {\n' % {'name': names[0]}

    for i in range(types.__len__()):
        result += '        %(sub_type)s %(sub_name)s = %(index)s;\n' % {'sub_name': names[i + 1], 'sub_type': meta_define[types[i]], 'index': i+1}

    result += '    }\n'
    result += '    repeated sub_%s %s = %s;  // %s\n' % (names[0], names[0], index, note)
    return result


def OutputProto(file_name, datas):
    base_name = datas.sheet_name
    if not datas.fields[0].is_client():  # or base_name == 'i18n' or base_name == 'UserAgreement':
        return
    class_name = base_name
    key_type = datas.fields[0].field_type
    key_array, _ = utils.ArrayCheck(key_type)
    if key_array:
        raise Exception("Error : key must be meta type" + base_name)

    field = ''
    b_import = False
    for col in range(datas.fields.__len__()):
        if not datas.fields[col].is_client():
            continue

        field_type = datas.fields[col].field_type
        field_name = datas.fields[col].field_name
        note = datas.fields[col].field_desc

        is_array, _ = utils.ArrayCheck(field_type)
        is_array_array, _ = utils.ArrayArrayCheck(field_type)
        if not is_array and not is_array_array:
            field += AddMetaFieldDeclare(field_name, field_type, col+1, note)
        elif is_array_array:
            b_import = True
            field += AddArrayArrayDeclare(field_name, field_type, col+1, note)
        else:
            field += AddArrayFieldDeclare(field_name, field_type, col+1, note)
            pass
    sz_import = protoTmp.pb_import_array if b_import else ''
    with open(file_name, 'w+', newline='', encoding='utf-8') as f:
        f.write(protoTmp.file_tmp % {'import_array': sz_import, 'name': class_name, 'field': field})
        pass
    GenPythonSource(file_name)
    GenBinary(datas)


def GenPythonSource(proto_file_path):
    '''
    https://github.com/lchannng/xlstool
    '''
    # cmd = "%s -I %s --python_out=%s %s"\
    #    % (PROTOC_BIN, PROTO_OUTPUT_PATH, PYTHON_OUTPUT_PATH, proto_file)
    PROTOC_BIN = ""
    if platform.system() == "Windows":
        PROTOC_BIN = os.path.join(os.getcwd(), "xlsParser/protoc/protoc-win/bin/protoc.exe")
    else:
        PROTOC_BIN = os.path.join(os.getcwd(), "xlsParser/protoc/protoc-osx/bin/protoc")
    pass
    args = [
        PROTOC_BIN,
        "-I", out_proto_path,
        "--python_out=%s" % out_py_path,
        proto_file_path
    ]
    subprocess.check_call(args, shell=False)


def parse_row(sheet_meta, row, item):
    # LOG_DEBUG("parsing row %s, id %s" % (row, item_id))
    for col in range(sheet_meta.fields.__len__()):
        if not sheet_meta.fields[col].is_client():
            continue
        field_name = sheet_meta.fields[col].field_name
        field_type = sheet_meta.fields[col].field_type
        field_value = sheet_meta.datas[row][col]

        is_array, _ = utils.ArrayCheck(field_type)
        is_array_array, _ = utils.ArrayArrayCheck(field_type)

        # single value
        if not is_array and not is_array_array:
            item.__setattr__(field_name, utils.GetFieldValue(field_value, field_type))
        # array of array
        elif is_array_array:
            field_type = field_type[:-4]  # remove [][]
            field_item = item.__getattribute__(field_name)
            value_arrays = utils.GetAsArrayArray(field_value)
            for index_1 in range(value_arrays.__len__()):
                sub_item = field_item.add()
                sub_values = utils.GetAsArray(value_arrays[index_1])
                for index_2 in range(sub_values.__len__()):
                    sub_item.__getattribute__('items').append(utils.GetFieldValue(sub_values[index_2], field_type))
                    pass
                pass
            pass
        else:
            field_type = field_type[:-2]  # remove []
            # array of single values
            if not utils.StruckCheck(field_type):
                value_array = utils.GetAsArray(field_value)
                for index in range(value_array.__len__()):
                    item.__getattribute__(field_name).append(utils.GetFieldValue(value_array[index], field_type))
                    pass
                pass
            # array of struct values
            else:
                value_names = field_name.split('|')
                value_types = field_type[1:-1].split('|')
                value_arrays = utils.GetAsArrayArray(field_value)
                for index_1 in range(value_arrays.__len__()):
                    struct_item = item.__getattribute__(value_names[0]).add()
                    sub_values = utils.GetAsArray(value_arrays[index_1])
                    for index_2 in range(sub_values.__len__()):
                        struct_item.__setattr__(value_names[index_2+1], utils.GetFieldValue(sub_values[index_2], value_types[index_2]))
                        pass
                    pass
                pass
            pass


def load_pymodule(struct_name):
    module_name = struct_name + "_pb2"
    exec("from %s import *" % module_name)
    module = sys.modules[module_name]
    return module


def GenBinary(metas):
    data_blocks_module = load_pymodule(metas.sheet_name)
    proto_data = getattr(data_blocks_module, metas.sheet_name + "List")()
    items = proto_data.__getattribute__("configs")
    for row in range(metas.datas.__len__()):
        item = items.add()
        parse_row(metas, row, item)
        pass
    pass
    data = proto_data.SerializeToString()
    bytes_path = out_binary_path + metas.get_proto_data_name()
    with open(bytes_path, "wb+") as f:
        f.write(data)
