import datetime
from enum import Enum


def ArrayCheck(str_type):
    if (str_type[-2:] == '[]'):
        return True, str_type[:-2]
    else:
        return False, str_type


def StruckCheck(str_type):
    return str_type[0] == '[' and str_type[-1] == ']'


def ArrayArrayCheck(str_type):
    if (str_type[-4:] == '[][]'):
        return True, str_type[:-4]
    else:
        return False, str_type


def GetFieldValue(value, type):
    if type == 'int':
        if isinstance(value, str):
            return GetAsInt(value)
        if isinstance(value, datetime.datetime):
            return int(value.timestamp())
        return value
    elif type == 'float':
        if isinstance(value, str):
            return GetAsFloat(value)
        return value
    elif type == 'bool':
        return GetAsBool(value)
    elif type == 'string':
        return GetAsString(value)
    else:
        raise Exception('complex type as meta type')


def GetAsInt(value):
    if value == '' or value.isspace():
        return 0
    return int(value, 0)


def GetAsFloat(value):
    if value == '' or value.isspace():
        return 0
    return float(value, 0)


def GetAsBool(value):
    if value == True or value == 'TRUE' or value == 'True' or value == 'true':
        return True
    return False


def GetAsString(value):
    if value == None:
        return ''
    return str(value)


def GetAsArray(value):
    if value == None or value == '':
        return []
    return str(value).split('|')


def GetAsArrayArray(value):
    if value == None or value == '':
        return []
    return value[1:-1].split(']|[')


def GetEnumDict(enum_define_sheet):
    result = {}
    for row in range(1, enum_define_sheet.max_row + 1):
        cell = enum_define_sheet._get_cell(row, 1)
        result[cell.value] = cell.row
    return result
