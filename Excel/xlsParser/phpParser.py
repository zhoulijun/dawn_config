import utils
import phpTmp


def GetIntParser(value):
    return str(utils.GetFieldValue(value, 'int'))


def GetFloatParser(value):
    return str(utils.GetFieldValue(value, 'float'))


def GetBoolParser(value):
    result = bool(value)
    result = result and 'true' or 'false'
    return result


def GetStringParser(value):
    return '\'%s\'' % str(value)


meta_parsers = {
    'int': GetIntParser,
    'float': GetFloatParser,
    'bool': GetBoolParser,
    'string': GetStringParser,
}


def AddMetaFieldInit(field_name, field_type, value):
    if not utils.StruckCheck(field_type):
        return '\n\t\t\'%(field_name)s\' => %(value)s,' % {'field_name': field_name, 'value': meta_parsers[field_type](value)}
    else:
        return AddStructFieldInit(field_name, field_type, value)


def AddStructFieldInit(field_name, field_type, value):
    names = field_name.split('|')
    types = field_type[1:-1].split('|')
    values = str(value)[1:-1].split('|')
    if names.__len__() != (types.__len__() + 1) or types.__len__() != values.__len__():
        raise Exception("struct fields and names mismatch!")

    str_fields = ''
    for i in range(types.__len__()):
        sub_field = names[i + 1]
        sub_value = meta_parsers[types[i]](values[i])
        str_fields += '\n\t\t\t\'%s\' => %s,' % (sub_field, sub_value)
    return '\n\t\t\'%s\' => [%s\n\t\t],' % (names[0], str_fields)


def AddArrayFieldInit(field_name, field_type, value):
    if not utils.StruckCheck(field_type):
        if not str(value).replace(' ', ''):
            return '\n\t\t\'%s\' => [],' % field_name
        str_value = ''
        value_array = str(value).split('|')
        for index in range(value_array.__len__()):
            var = meta_parsers[field_type](value_array[index])
            if index == 0:
                str_value += var
            else:
                str_value += (', ' + var)
            pass
        return '\n\t\t\'%s\' => [%s],' % (field_name, str_value)
    else:
        return AddStructArrayFieldInit(field_name, field_type, value)


def AddStructArrayFieldInit(field_name, field_type, value):
    names = field_name.split('|')
    types = field_type[1:-1].split('|')
    values = str(value)[1:-1].split(']|[')
    if names.__len__() != (types.__len__() + 1):
        raise Exception("struct fields and names mismatch!")

    str_fields = ''
    for i in range(values.__len__()):
        struct_values = values[i].split('|')
        str_struct = ''
        # 空单元格会导致结果不匹配,直接跳过
        if values[i] == '':
            continue
        pass
        for field_i in range(types.__len__()):
            str_struct += '\n\t\t\t\t\'%s\' => %s,' % (
                names[field_i + 1], meta_parsers[types[field_i]](struct_values[field_i]))
        str_fields += '\n\t\t\t%s => [%s\n\t\t\t],' % (i, str_struct)
    return '\n\t\t\'%s\' => [%s\n\t\t],' % (names[0], str_fields)


def AddArrayArrayFieldInit(field_name, field_type, value):
    if not str(value).replace(' ', ''):
        return '\n\t\t\'%s\' => [],' % field_name
    values = str(value)[1:-1].split(']|[')
    str_fields = ''
    for row in range(values.__len__()):
        str_row = ''
        row_datas = values[row].split('|')
        for col in range(row_datas.__len__()):
            var = meta_parsers[field_type](row_datas[col])
            if col == 0:
                str_row += var
            else:
                str_row += (', ' + var)
            pass
        str_fields += '\n\t\t\t%s => [%s],' % (row, str_row)
    return '\n\t\t\'%s\' => [%s\n\t\t],' % (field_name, str_fields)


def OutputPhp(file_name, datas):
    if not datas.fields[0].is_server():
        return
    # 注释
    notes = ''
    for col in range(datas.fields.__len__()):
        if not datas.fields[col].is_server():
            continue
        notes += '\n//%s:%s' % (datas.fields[col].field_name, datas.fields[col].field_desc)
        pass
    # 具体的值
    str_row = '\n\t%(row_key)s => [%(values)s\n\t],'
    config_values = ''
    for row_index in range(0, datas.datas.__len__()):
        row_key_type = datas.fields[0].field_type
        row_key = meta_parsers[row_key_type](datas.datas[row_index][0])
        values = ''
        try:
            for col_index in range(datas.datas[row_index].__len__()):
                if not datas.fields[col_index].is_server():
                    continue
                field_type = datas.fields[col_index].field_type
                field_name = datas.fields[col_index].field_name
                field_value = datas.datas[row_index][col_index]

                is_array_array, field_type = utils.ArrayArrayCheck(
                    field_type)
                is_array, field_type = utils.ArrayCheck(field_type)
                is_struct = utils.StruckCheck(field_type)
                if not is_struct and not meta_parsers.__contains__(field_type):
                    raise Exception('unknown field meta type : ' + field_type + ', fieldName : ' + field_name)
                if is_array_array and is_struct:
                    raise Exception('no support array_array for struct')
                elif not is_array and not is_array_array:
                    values += AddMetaFieldInit(field_name, field_type, field_value)
                elif is_array:
                    values += AddArrayFieldInit(field_name, field_type, field_value)
                else:
                    values += AddArrayArrayFieldInit(field_name, field_type, field_value)
                pass
            pass
        except ValueError:
            raise Exception('错误发生在第%s行' % (row_index + 5))
        config_values += str_row % {'row_key': row_key, 'values': values}
        pass
    php_value = phpTmp.file_tmp
    with open(file_name, 'w+', newline='', encoding='utf-8') as f:
        f.write(php_value % {'notes': notes, 'values': config_values})
    pass
