import os
import shutil

php_path = os.path.join(os.getcwd(), '../../atw_server/config/game/php')
data_path = os.path.join(os.getcwd(), '../../atw_client/proj_unity/Assets/StreamingAssets/GDS')
csharp_path = os.path.join(os.getcwd(), '../../atw_client/proj_unity/Assets/Script/GDS')

# shutil.move('./output/php', php_path)
# shutil.move('./output/datas', data_path)
# shutil.move('./output/csharp', csharp_path)

for root, dirs, files in os.walk('./output/php'):
    for file in files:
        shutil.copy(os.path.join(root, file), php_path)

for root, dirs, files in os.walk('./output/datas'):
    for file in files:
        shutil.copy(os.path.join(root, file), data_path)

for root, dirs, files in os.walk('./output/csharp'):
    for file in files:
        shutil.copy(os.path.join(root, file), csharp_path)
