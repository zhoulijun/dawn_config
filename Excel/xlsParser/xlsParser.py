# -*- coding: utf-8 -*-
import os
import sys
import shutil
from openpyxl import load_workbook

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database

import protoTmp
import csharpParser
import phpParser
import protoParser
import utils

excel_path = os.path.join(os.getcwd(), '')

out_php_path = os.path.join(excel_path, 'output/php')
out_py_path = os.path.join(excel_path, 'output/py')
out_datas_path = os.path.join(excel_path, 'output/datas')
out_proto_path = os.path.join(excel_path, "output/proto")
out_csharp_path = os.path.join(excel_path, 'output/csharp')

# row 1: sc cs s c   s <=> server c <=> client
# row 2: type of col. e.g:  int bool string int[]
# row 3: name of col.
# row 4: note of the col
# row 5-max: datas


class FieldInfo:
    '''
    Col Info. 
    '''

    def __init__(self, flag, ftype, fname, fdesc):
        self.flag = flag
        self.field_type = ftype
        self.field_name = fname
        self.field_desc = fdesc

    def is_client(self):
        return self.flag == 'c' or self.flag == 'sc' or self.flag == 'cs'

    def is_server(self):
        return self.flag == 's' or self.flag == 'sc' or self.flag == 'cs'


class SheetData:
    '''
    Excel datas.
    Only read first sheet of excel.
    '''

    def __init__(self, sheet_name):
        self.sheet_name = sheet_name
        self.fields = []
        self.datas = []

    def set_sheet_data(self, worksheet_datas):
        self.datas = worksheet_datas

    def get_proto_data_name(self):
        return "/" + self.sheet_name + ".dat"

    def get_csv_name(self):
        return "/" + self.sheet_name + ".csv"

    def get_proto_name(self):
        return "/" + self.sheet_name + ".proto"

    def get_cs_name(self):
        return "/" + self.sheet_name + "Config.cs"

    def get_php_name(self):
        return "/" + self.sheet_name + "Config.php"


def XlsxReader(file_name):
    baseName = file_name.split('.')[0]
    # if baseName != 'UserAgreement':
    #     return
    wb = load_workbook(excel_path + '/' + file_name, data_only=True)
    sheet_first = wb.worksheets[0]

    sheet_datas = SheetData(baseName)

    fields = []
    for col in range(1, sheet_first.max_column + 1):
        flag = sheet_first._get_cell(1, col).value
        field_type = sheet_first._get_cell(2, col).value
        field_name = sheet_first._get_cell(3, col).value
        field_desc = sheet_first._get_cell(4, col).value

        if sheet_first._get_cell(1, col).value == None or sheet_first._get_cell(2, col).value == 'note':
            continue
        # enum will be saved as int when being parsed
        if field_type == 'enum':
            field_type = 'int'
        fields.append(FieldInfo(flag, field_type, field_name, field_desc))
    sheet_datas.fields = fields

    # read datas from xlsx
    sheet_data = []
    for row in range(5, sheet_first.max_row + 1):
        if sheet_first._get_cell(row, 1).value == None:
            continue
        sheet_data.append([])

    for col in range(1, sheet_first.max_column + 1):
        if sheet_first._get_cell(1, col).value == None:
            continue
        type_def_cell = sheet_first._get_cell(2, col)
        if type_def_cell.value == 'note':
            continue
        # enum prepare
        b_enum = False
        enum_dict = {}
        if type_def_cell.value == 'enum':
            b_enum = True
            string_index_cell = type_def_cell.column_letter
            enum_sheet = wb[string_index_cell]
            if enum_sheet == None:
                raise Exception("enum's define does not exist!")
            enum_dict = utils.GetEnumDict(enum_sheet)
        # enum prepare end
        cur_row = 0
        for row in range(5, sheet_first.max_row + 1):
            if sheet_first._get_cell(row, 1).value == None:
                continue
            cell_value = sheet_first._get_cell(row, col).value
            if b_enum:
                if cell_value == None:
                    cell_value = 0
                else:
                    cell_value = enum_dict[cell_value]
            if cell_value == None:
                cell_value = ''
            sheet_data[cur_row].append(cell_value)
            cur_row = cur_row + 1
            pass
        pass
    sheet_datas.set_sheet_data(sheet_data)

    # server
    # php gen
    phpParser.OutputPhp(out_php_path + sheet_datas.get_php_name(), sheet_datas)

    # client
    # proto gen
    protoParser.OutputProto(out_proto_path + sheet_datas.get_proto_name(), sheet_datas)
    # csharp gen
    csharpParser.OutputCSharp(out_csharp_path + sheet_datas.get_cs_name(), sheet_datas)


# 检查生成路径是否存在,不存在则创建
if os.path.exists(out_php_path):
    shutil.rmtree(out_php_path)
os.makedirs(out_php_path)

if os.path.exists(out_csharp_path):
    shutil.rmtree(out_csharp_path)
os.makedirs(out_csharp_path)

if os.path.exists(out_proto_path):
    shutil.rmtree(out_proto_path)
os.makedirs(out_proto_path)

if os.path.exists(out_py_path):
    shutil.rmtree(out_py_path)
os.makedirs(out_py_path)

if os.path.exists(out_datas_path):
    shutil.rmtree(out_datas_path)
os.makedirs(out_datas_path)


# genPbArray.proto
common_proto_path = out_proto_path + '/PbArray.proto'
with open(common_proto_path, 'w+', newline='', encoding='utf-8') as f:
    f.write(protoTmp.pb_array_tmp)
protoParser.GenPythonSource(common_proto_path)
sys.path.append(out_py_path)

print("开始生成")
for target_file in os.listdir(excel_path):
    if os.path.splitext(target_file)[1] == '.xlsx':  # and target_file == 'MapResource.xlsx':
        if not target_file.startswith("$") and not target_file.startswith("~$"):
            print("开始转换" + target_file)
            XlsxReader(target_file)
            pass
        pass
    pass
