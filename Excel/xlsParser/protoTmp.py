file_tmp = '''syntax = "proto3";
%(import_array)s
message %(name)sList {
    repeated %(name)sConfig configs = 1;
}

message %(name)sConfig {
%(field)s}
'''

pb_array_tmp = '''syntax = "proto3";

message PBintArray  {
    repeated int32 items = 1;
}

message PBfloatArray  {
    repeated float items = 1;
}

message PBboolArray{
    repeated bool items = 1;
}

message PBstringArray{
    repeated string items = 1;
}
'''

pb_import_array = '''
import "PbArray.proto";
'''