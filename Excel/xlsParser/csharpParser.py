import os
import platform
import subprocess
import getpass

import csharpTmp
import utils

out_proto_path = os.path.join(os.getcwd(), "output/proto")
out_csharp_path = os.path.join(os.getcwd(), "output/csharp")


def GenCSharpProtobufNetSource(file_name):
    '''
    https://github.com/protobuf-net/protobuf-net
    '''
    PROTO_GEN = ""
    if platform.system() == "Windows":
        PROTO_GEN = os.path.join(os.getcwd(), "xlsParser/protogen/net462/protogen.exe")
    else:
        PROTO_GEN = "/Users/" + getpass.getuser() + "/.dotnet/tools/protogen"
    pass
    args = [
        PROTO_GEN,
        "--proto_path=%s" % out_proto_path+"/",
        file_name,
        "--csharp_out=%s" % out_csharp_path,
        "+langver=3",
        "+names=original",
    ]
    subprocess.check_call(args, shell=False)


def OutputCSharp(file_name, datas):
    if not datas.fields[0].is_client():
        return
    base_name = datas.sheet_name
    GenCSharpProtobufNetSource(base_name+'.proto')
    if base_name == 'i18n' or base_name == 'i18nLogin':
        return
    class_name = base_name + "Config"
    key_name = datas.fields[0].field_name
    key_type = datas.fields[0].field_type
    key_array, _ = utils.ArrayCheck(key_type)
    if key_array:
        raise Exception("Error : key must be meta type" + base_name)

    with open(file_name, 'w+', newline='', encoding='utf-8') as f:
        f.write(csharpTmp.file_tmp % {'class_name': class_name, 'key_type': key_type, 'key_name': key_name, 'base_name': base_name})
        pass
    pass
