file_tmp = '''using System.Collections.Generic;
using UnityEngine;

public partial class %(class_name)s
{
    private static Dictionary<%(key_type)s, %(class_name)s> configs = new Dictionary<%(key_type)s, %(class_name)s>();
    public static Dictionary<%(key_type)s, %(class_name)s> Configs
    {
        get
        {
            if (!is_init) Init();
            return configs;
        }
    }

    public static %(class_name)s GetRow(%(key_type)s id)
    {
        Configs.TryGetValue(id, out %(class_name)s result);

        return result;
    }

    static bool is_init = false;
    public static void Init()
    {
        is_init = true;
        configs.Clear();
        byte[] text = FileManager.Instance.ReadAllBytes("GDS/%(base_name)s.dat");
        System.IO.MemoryStream stream = new System.IO.MemoryStream(text);
        var dicts = ProtoBuf.Serializer.Deserialize<%(base_name)sList>(stream);
        foreach (var item in dicts.configs)
        {
#if UNITY_EDITOR
            if (configs.ContainsKey(item.%(key_name)s))
            {
                string error = "重复的key: %(base_name)s.dat : " + item.%(key_name)s;
                Debug.LogError(error);
                throw new System.Exception(error);
            }
#endif
            configs[item.%(key_name)s] = item;
        }
    }

    public static void Clear()
    {
        is_init = false;
        configs.Clear();
    }
}
'''
